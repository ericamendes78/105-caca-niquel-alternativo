package main;

public class Calculador {
	private int[] contagem;
			
	public int calcular(int[] sorteios) {
		contagem = new int[Rolo.getTamanho()];
		
		for(int i = 0; i < contagem.length; i ++) {
			contagem[i] = 0;
		}
		
		for(int numero: sorteios) {
			contagem[numero]++;
		}
		
		for(int i = 0; i < contagem.length; i++) {
			int casa = contagem[i];
					
			if(casa == 3) {
				if(i == Rolo.valorEspecial) {
					return 5000;
				}
				
				return 1000;
			}
			
			if(casa == 2) {
				return 100;
			}
		}
		
		return calcularPontuacao(contagem);
	}
	
	private int calcularPontuacao(int[] contagem) {
		int maiorContagem = 0;
		int maiorValor = 0;
		
		for(int i = 0; i < contagem.length; i++) {
			if(contagem[i] > maiorContagem) {
				maiorValor = i;
				maiorContagem = contagem[i];
			}
		}
		
		if(maiorContagem >= 3) {
			if(maiorValor == Rolo.valorEspecial) {
				return 5000;
			}
			return 1000;
		}
		
		if(maiorContagem == 2) {
			return 100;
		}
		
		return 0;
	}
}
